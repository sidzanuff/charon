﻿namespace Charon
{
    internal class Handshake
    {
        public string type;
        public string transferId;
        public long length;
    }
}