﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System;
using System.IO;
using System.Linq;

namespace Charon
{
    internal class Program
    {
        private static string dir = ".\\";
        private static string uri = "http://localhost:3000";
        //private static string uri = "http://149.202.52.131:3000";

        private static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                dir = args[0];
            }
            if (args.Length > 1)
            {
                uri = args[1];
            }

            var socket = IO.Socket(uri);

            socket.Emit("handshake", JsonConvert.SerializeObject(new Handshake { type = "meta" }));

            socket.On("get list", () =>
            {
                Log("get list");
                var sharedFiles = Directory.GetFiles(dir)
                .Select(f => new SharedFileInfo
                {
                    name = f.Replace(dir, ""),
                    length = new FileInfo(f).Length
                }).ToList();
                socket.Emit("list", JsonConvert.SerializeObject(sharedFiles));
            });

            socket.On("connected", data =>
            {
                Log("assigned id: {0}", data);
            });

            socket.On("request", data =>
            {
                var request = (JObject)data;
                var id = (string)request["id"];
                var name = (string)request["name"];
                Log("upload #{0}:{1} started", id, name);
                var path = Path.Combine(dir, name);
                var stream = File.OpenRead(path);
                var transferSocket = IO.Socket(uri);
                transferSocket.Emit("handshake", JsonConvert.SerializeObject(new Handshake()
                {
                    type = "data",
                    length = new FileInfo(path).Length,
                    transferId = id
                }));
                transferSocket.On("ack", () =>
                {
                    var buffer = new byte[4096];
                    var count = stream.Read(buffer, 0, buffer.Length);
                    var finished = count < buffer.Length;
                    if (finished)
                    {
                        Array.Resize(ref buffer, count);
                        Log("upload #{0} complete", id);
                        stream.Close();
                    }
                    //Log("upload #{0} sending {1}", id, buffer.Length);
                    transferSocket.Emit("upload", buffer);
                    if (finished)
                    {
                        transferSocket.Close();
                    }
                });
            });

            Log("sharing {0} to server at {1}.", dir, uri);
            Log("press enter to close...");
            Console.ReadLine();
        }

        private static void Log(string msg, params object[] args)
        {
            Console.WriteLine(DateTime.Now + " :: " + msg, args);
        }
    }
}