var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var UUID = require('node-uuid');
var async = require('async');

var clients = {};
var transfers = {};

http.listen(3000, function () {
    console.log('listening on *:3000');
});

app.get('/', function (req, res) {
    var body = '<ul>';
    async.forEachOf(clients, function (socket, clientId, callback) {
        socket.emit("get list");
        socket.once("list", function (data) {
            var fileList = JSON.parse(data);
            for (var i = 0; i < fileList.length; i++) {
                var file = fileList[i];
                body += '<li><a href="' + clientId + '/' + file.name + '">' + clientId + '/' + file.name
                    + ' (' + file.length + ')</a></li>';
            }
            callback();
        });
    }, function () {
        body += '</ul>';
        res.send(body);
    });
});

app.get('/:clientId/:fileName', function (req, res) {
    var clientId = req.params.clientId;
    var fileName = req.params.fileName;
    var socket = clients[clientId];
    if (socket === undefined) {
        console.log('client not found ' + clientId);
        return;
    }
    var transferId = UUID();
    transfers[transferId] = res;
    socket.emit("request", {name: fileName, id: transferId});
});

io.on('connection', function (socket) {
    socket.on("handshake", function (data) {
        var handshake = JSON.parse(data);
        if (handshake.type === "meta") {
            var clientId = UUID();
            clients[clientId] = socket;
            socket.emit("connected", clientId);
            socket.on('disconnect', function () {
                delete clients[clientId];
            });
        } else {
            var id = handshake.transferId;
            console.log(id + ' started');
            var res = transfers[id];
            res.writeHead(200, {
                'Content-Type': 'application/octet-stream',
                'Content-Length': handshake.length
            });
            var uploaded = 0;
            socket.on('upload', function (data) {
                res.write(data);
                uploaded += data.length;
                //console.log(id + ' received ' + uploaded);
                if (uploaded == handshake.length) {
                    console.log(id + ' finished');
                    res.end();
                    delete transfers[id];
                } else {
                    socket.emit("ack");
                }
            });
            socket.emit('ack');
        }
    });
});